﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelExport
{
    internal static class ExtensionMethods
    {
        static ExtensionMethods() { }
        internal static string ToExcelColumDescription(this int column)
        {
            //ToDo @Zukunfts-Ruv: Schreib gefälligst einen ordenlichen Algorythmus für zwei stellige Column Bezeichnungen - Pisser!
            int ansiiStart = 64;

            int size = Convert.ToInt32(Math.Ceiling(column / 26.0));
            int[] arr = new int[size];

            string result = string.Empty;

            if (arr.Length == 1)
                result = Convert.ToChar(column + ansiiStart).ToString().ToUpper();

            return result;
        }
    }
}
