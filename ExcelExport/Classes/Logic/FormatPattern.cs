﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OfficeOpenXml.Style;

namespace ExcelExport
{
    public class FormatPattern
    {
        public FormatPattern() { }

        #region Properties
        public Font Font
        {
            set
            {
                _font = value;
            }
            get
            {
                return _font;
            }
        }
        private Font _font;

        public Cell.EnValueFormat ValueFormat
        {
            set
            {
                _valueFormat = value;
            }
            get
            {
                return _valueFormat;
            }
        }
        private Cell.EnValueFormat _valueFormat;

        public Color FontColor
        {
            set
            {
                _fontColor = value;
            }
            get
            {
                return _fontColor;
            }
        }
        private Color _fontColor;

        public Color BackGroundColor
        {
            set
            {
                _backGroundColor = value;
            }
            get
            {
                return _backGroundColor;
            }
        }
        private Color _backGroundColor;

        public OfficeOpenXml.Style.ExcelBorderStyle Frame
        {
            get
            {
                return _frame;
            }

            set
            {
                _frame = value;
            }
        }
        private OfficeOpenXml.Style.ExcelBorderStyle _frame;
        #endregion
    }
}
