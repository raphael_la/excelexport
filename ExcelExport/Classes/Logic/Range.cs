﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelExport
{
    public class Range
    {
        #region Constructors
        private Range()
        {
            _columnX = null;
            _columnY = null;
            _rowX = null;
            _rowY = null;
        }
        private Range(int? columnX, int? columnY, long? rowX, long? rowY) : this()
        {
            this._columnX = columnX;
            this._columnY = columnY;
            this._rowX = rowX;
            this._rowY = rowY;
        }
        public Range(int columnX, int columnY, long rowX, long rowY) : this((int?)columnX, columnY, rowX, rowY) { }
        public Range(int columnX, int columnY, long rowX) : this(columnX, columnY, rowX, null) { }
        public Range(int columnX, long rowX, long rowY) : this(columnX, null, rowX, rowY) { }
        #endregion

        #region Properties
        private int? _columnX;
        public int? ColumnX
        {
            get
            {
                return _columnX;

            }
            set
            {
                _columnX = value;
            }
        }

        private int? _columnY;
        public int? ColumnY
        {
            get
            {
                return _columnY;
            }
        }

        private long? _rowX;
        public long? RowX
        {
            get
            {
                return _rowX;
            }
        }

        private long? _rowY;
        public long? RowY
        {
            get
            {
                return _rowY;
            }
        }
        #endregion

        #region Methods
        public static Range CreateRange(IEnumerable<Cell> cells, Table table)
        {
            if (cells == null || cells.Count() < 1)
                throw new ArgumentNullException("cells");

            int? maxCol = cells.OrderByDescending(x => x.Column)?.FirstOrDefault().Column;
            int? minCol = cells.OrderBy(x => x.Column)?.FirstOrDefault().Column;
            int? maxRow = cells.OrderByDescending(x => x.Row)?.FirstOrDefault().Row;
            int? minRow = cells.OrderBy(x => x.Row)?.FirstOrDefault().Row;

            if (minCol == maxCol && minRow == maxRow)
                throw new ArgumentException("no range - its a cell");

            table.Cells.AddRange(cells.Where(x => !table.Cells.Contains(x)));

            return new Range(minCol, maxCol, minRow, maxRow);
        }
        #endregion
    }
}
