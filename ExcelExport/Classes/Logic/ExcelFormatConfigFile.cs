﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Drawing;

namespace ExcelExport
{
    public static class ExcelFormatConfigFile
    {
        static ExcelFormatConfigFile()
        {
            CreateConfigXmlFile();
        }

        private static readonly string filePath = Path.Combine(ExcelExport.ExcelFile.ApplicationPath, "FormattingPatterns.xml");

        private static void CreateConfigXmlFile()
        {
            if (!System.IO.File.Exists(filePath))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<FormattingPatterns><pattern><id>Default</id></pattern></FormattingPatterns>");

                XmlTextWriter writer = new XmlTextWriter(filePath, null);
                writer.Formatting = Formatting.Indented;
                doc.Save(writer);

                writer.Dispose();
            }
        }
        public static FormatPattern GetFormatPattern(string id)
        {
            XDocument xmlFile = XDocument.Load(filePath);
            FormatPattern formatPattern = new FormatPattern();
            ColorConverter colorConverter = new ColorConverter();
            FontConverter fontConverter = new FontConverter();
            var node = xmlFile.Descendants("pattern").Where(x => x.Element("id")?.Value == id)?.FirstOrDefault();

            formatPattern.BackGroundColor = (Color)colorConverter.ConvertFromString(node.Element("BackGroundColor")?.Value);
            formatPattern.Font = (Font)fontConverter.ConvertFromString(node.Element("Font")?.Value);
            formatPattern.FontColor = (Color)colorConverter.ConvertFromString(node.Element("FontColor")?.Value);
            formatPattern.ValueFormat = (Cell.EnValueFormat)Enum.Parse(typeof(Cell.EnValueFormat), node.Element("ValueFormat")?.Value);
            formatPattern.Frame = (OfficeOpenXml.Style.ExcelBorderStyle)Enum.Parse(typeof(OfficeOpenXml.Style.ExcelBorderStyle), node.Element("Frame")?.Value);

            return formatPattern;
        }
    }
}
