﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ExcelExport
{
    public class Cell
    {
        #region Construcor(s)
        public Cell()
        {
            _column = 0;
            _row = 0;
            _valueFormat = EnValueFormat.String;
            _value = null;
            _fontColor = Color.Black;
            _backGroundColor = Color.White;
            _frame = ExcelBorderStyle.None;
        }
        public Cell(int column, int row) : this()
        {
            if (column <= 0 || row <= 0)
                throw new ArgumentNullException("Cell");

            this._column = column;
            this._row = row;
        }
        public Cell(int column, int row, string value) : this(column, row)
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException("value");

            _value = value;
        }
        #endregion

        #region Properties
        public int Column
        {
            get
            {
                return _column;
            }
        }
        private int _column;

        public int Row
        {
            get
            {
                return _row;
            }
        }
        private int _row;

        public Font Font
        {
            set
            {
                _font = value;
            }
            get
            {
                return _font;
            }
        }
        private Font _font;

        public EnValueFormat ValueFormat
        {
            set
            {
                _valueFormat = value;
            }
            get
            {
                return _valueFormat;
            }
        }
        private EnValueFormat _valueFormat;

        public string Value
        {
            set
            {
                _value = value.ToString();
            }
            get
            {
                return _value;
            }
        }
        public double? ValueDouble
        {
            get
            {
                //ToDo ZahlenFormat Berücksichtigen!
                double d;
                double.TryParse(_value, out d);
                return d;
            }
        }
        private string _value;

        public Color FontColor
        {
            set
            {
                _fontColor = value;
            }
            get
            {
                return _fontColor;
            }
        }
        private Color _fontColor;

        public Color BackGroundColor
        {
            set
            {
                _backGroundColor = value;
            }
            get
            {
                return _backGroundColor;
            }
        }
        private Color _backGroundColor;

        public OfficeOpenXml.Style.ExcelBorderStyle Frame
        {
            get
            {
                return _frame;
            }

            set
            {
                _frame = value;
            }
        }
        private OfficeOpenXml.Style.ExcelBorderStyle _frame;

        public FormatPattern FormatPattern
        {
            get
            {
                return _formatPattern;
            }

            set
            {
                _formatPattern = value;
                if (_formatPattern != null)
                {
                    this.BackGroundColor = _formatPattern.BackGroundColor;
                    this.Font = _formatPattern.Font;
                    this.FontColor = _formatPattern.FontColor;
                    this.Frame = _formatPattern.Frame;
                    this.ValueFormat = _formatPattern.ValueFormat;
                }
            }
        }
        private FormatPattern _formatPattern;
        #endregion

        #region Enums
        public enum EnValueFormat
        {
            String,
            Zahl0,
            Zahl1,
            Zahl2,
            Zahl3
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return "S" + _column + " Z" + _row + ": " + _value;
        }
        #endregion

        #region Operators
        public static bool operator ==(Cell z1, Cell z2)
        {
            return z1._column == z2._column && z1._row == z2._row;
        }
        public static bool operator !=(Cell z1, Cell z2)
        {
            return z1._column != z2._column || z1._row != z2._row;
        }
        public override bool Equals(object o)
        {
            return o == this;
        }
        #endregion
    }
}
