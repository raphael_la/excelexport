﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelExport
{
    public class Document
    {
        #region Constructor(s)
        private Document()
        {
            _name = null;
            _path = null;
            _tables = new List<Table>();
        }
        public Document(string name, string path) : this()
        {
            if (!ExcelExport.ExcelFile.IsPathValid(ref path))
                throw new ArgumentException("pathname is not valid");

            _name = name;
            _path = path;
        }
        #endregion

        #region Properties
        private List<Table> _tables;
        public List<Table> Tables
        {
            get
            {
                return _tables;
            }
            set
            {
                _tables = value;
            }
        }

        private string _path;
        public string Path
        {
            get
            {
                return _path;
            }

            set
            {
                _path = value;
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
        }
        #endregion

        #region Methods
        public Table AddTable(string name)
        {
            Table table = new Table(name);
            this.Tables.Add(table);
            return table;
        }
        public Table GetTabelleByName(string name) => this.Tables.Where(x => x.Name == name)?.FirstOrDefault();
        public void Save() => ExcelExport.ExcelFile.Save(this);
        #endregion
    }
}
