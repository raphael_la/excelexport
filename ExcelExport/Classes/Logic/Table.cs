﻿using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelExport
{
    public class Table
    {
        #region Constructor(s)
        private Table()
        {
            _name = null;
            DefaultFont = null;
            _cells = new List<Cell>();
            _columnWidths = new Dictionary<int, int>();
            _rowHights = new Dictionary<long, int>();
            _frames = new Dictionary<Range, ExcelBorderStyle>();
        }
        public Table(string name) : this()
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new NullReferenceException("name");
            _name = name;
        }
        #endregion

        #region Constants
        //ToDo DefaultWerte berücksichtigen
        private const int DEFAULT_ColumnNBREITE = 150;
        private const int DEFAULT_rowNHOEHE = 25;
        #endregion

        #region Properties
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        private bool _saveTableFormatToXML;
        public bool SaveFormatToXML
        {
            get
            {
                return _saveTableFormatToXML;
            }

            set
            {
                _saveTableFormatToXML = value;
            }
        }

        public List<Cell> Cells
        {
            get
            {
                return _cells;
            }

            set
            {
                _cells = value;
            }
        }
        private List<Cell> _cells;

        internal Dictionary<int, int> ColumnWidths
        {
            get
            {
                return _columnWidths;
            }

            set
            {
                _columnWidths = value;
            }
        }
        private Dictionary<int, int> _columnWidths;

        internal Dictionary<long, int> RowHights
        {
            get
            {
                return _rowHights;
            }

            set
            {
                _rowHights = value;
            }
        }
        private Dictionary<long, int> _rowHights;

        internal Dictionary<Range, ExcelBorderStyle> Frames
        {
            get
            {
                return _frames;
            }

            set
            {
                _frames = value;
            }
        }
        private Dictionary<Range, OfficeOpenXml.Style.ExcelBorderStyle> _frames;

        public Font DefaultFont
        {
            get
            {
                if (_defaultFont == null)
                    return new Font("Arial", 12.0f);
                return _defaultFont;
            }

            set
            {
                _defaultFont = value;
            }
        }
        private Font _defaultFont;
        #endregion

        #region Methods

        public IEnumerable<Cell> GetCells(Range range) => this.Cells.Where(x => x.Row >= range.RowX && x.Row <= range.RowY && x.Column >= range.ColumnX && x.Column <= range.ColumnY);

        #region SetFont
        public void SetFont(IEnumerable<Range> range, Font font) => range.ToList().ForEach(x => SetFont(x, font));
        public void SetFont(Range range, Font font) => GetCells(range).ToList().ForEach(x => SetFont(x, font));
        public void SetFont(IEnumerable<Cell> cells, Font font) => cells.ToList().ForEach(x => SetFont(x, font));
        public void SetFont(Cell cell, Font font) => cell.Font = font;
        #endregion

        #region SetFontColor
        public void SetFontColor(IEnumerable<Range> range, Color color) => range.ToList().ForEach(x => SetFontColor(x, color));
        public void SetFontColor(Range range, Color color) => GetCells(range).ToList().ForEach(x => SetFontColor(x, color));
        public void SetFontColor(IEnumerable<Cell> cells, Color color) => cells.ToList().ForEach(x => SetFontColor(x, color));
        public void SetFontColor(Cell cell, Color color) => cell.FontColor = color;
        #endregion

        #region SetBackGroundColor
        public void SetBackGroundColor(IEnumerable<Range> range, Color color) => range.ToList().ForEach(x => SetBackGroundColor(x, color));
        public void SetBackGroundColor(Range range, Color color) => GetCells(range).ToList().ForEach(x => SetBackGroundColor(x, color));
        public void SetBackGroundColor(IEnumerable<Cell> cells, Color color) => cells.ToList().ForEach(x => SetBackGroundColor(x, color));
        public void SetBackGroundColor(Cell cell, Color color) => cell.BackGroundColor = color;
        #endregion

        #region SetFrame
        public void SetFrame(IEnumerable<Range> range, OfficeOpenXml.Style.ExcelBorderStyle frameStyle) => range.ToList().ForEach(x => SetFrame(x, frameStyle));
        public void SetFrame(Range range, OfficeOpenXml.Style.ExcelBorderStyle frameStyle)
        {
            if (!this.Frames.Keys.Contains(range))
                Frames.Add(range, frameStyle);
        }
        #endregion

        #region SetColumWidth
        public void SetColumnWidth(int index, int width)
        {
            if (!ColumnWidths.Keys.Contains(index))
                ColumnWidths.Add(index, width);
        }
        public void SetColumnWidth(int[] index, int width) => index.ToList().ForEach(x => SetColumnWidth(x, width));
        public void SetColumnWidth(IEnumerable<Cell> cells, int width) => cells.ToList().ForEach(x => SetColumnWidth(x.Column, width));
        public void SetColumnWidth(Cell cell, int width) => SetColumnWidth(cell.Column, width);
        #endregion

        #region SetRowHight
        public void SetRowHight(long index, int height)
        {
            if (!RowHights.Keys.Contains(index))
                RowHights.Add(index, height);
        }
        public void SetRowHight(int[] index, int height) => index.ToList().ForEach(x => SetRowHight(x, height));
        public void SetRowHight(IEnumerable<Cell> cells, int height) => cells.ToList().ForEach(x => SetRowHight(x.Row, height));
        public void SetRowHight(Cell cell, int height) => SetRowHight(cell.Row, height);
        #endregion

        #region SetFormatPattern
        public void SetFormatPattern(Cell cell, FormatPattern pattern)
        {
            if (!this.Cells.Contains(cell))
                this.Cells.Add(cell);

            cell.FormatPattern = pattern;
        }
        public void SetFormatPattern(IEnumerable<Cell> cells, FormatPattern pattern) => cells.ToList().ForEach(x => SetFormatPattern(x, pattern));
        public void SetFormatPattern(Range range, FormatPattern pattern) => SetFormatPattern(this.GetCells(range), pattern);
        #endregion

        #endregion
    }
}
