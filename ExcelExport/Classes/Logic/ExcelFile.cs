﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using OfficeOpenXml;


namespace ExcelExport
{
    internal static class ExcelFile
    {
        #region Constructor(s)
        static ExcelFile() { }
        #endregion

        #region Properties
        public static string ApplicationPath
        {
            get
            {
                return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
        }
        #endregion

        #region Methods
        public static void Save(Document document)
        {
            const string filePostFix = ".xlsx";

            FileInfo newFile = new FileInfo(document.Path + document.Name + filePostFix);
            ExcelPackage pck = new ExcelPackage(newFile);

            List<Table> tables = document.Tables;
            foreach (Table tab in tables)
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(tab.Name);

                //Set Values
                foreach (Cell cell in tab.Cells)
                {
                    string field = cell.Column.ToExcelColumDescription() + cell.Row.ToString();
                    ws.Cells[field].Value = cell.Value;
                    ws.Cells[field].Style.Font.SetFromFont(cell.Font ?? tab.DefaultFont);
                    //ToDo Property in Cell FrameColor
                    if (cell.Frame != OfficeOpenXml.Style.ExcelBorderStyle.None)
                        ws.Cells[field].Style.Border.BorderAround(cell.Frame, System.Drawing.Color.Black);
                    ws.Cells[field].Style.Font.Color.SetColor(cell.FontColor);
                    ws.Cells[field].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[field].Style.Fill.BackgroundColor.SetColor(cell.BackGroundColor);
                }

                //Set Column Widths
                foreach (KeyValuePair<int, int> col in tab.ColumnWidths)
                    ws.Column(col.Key).Width = col.Value;

                //Set Row Hights
                foreach (KeyValuePair<long, int> row in tab.RowHights)
                    ws.Row(Convert.ToInt32(row.Key)).Height = row.Value;

                //Draw Frames / Borders
                foreach (KeyValuePair<Range, OfficeOpenXml.Style.ExcelBorderStyle> frame in tab.Frames)
                {
                    int colX = Convert.ToInt32(frame.Key.ColumnX);
                    int colY = Convert.ToInt32(frame.Key.ColumnY);
                    int rowX = Convert.ToInt32(frame.Key.RowX);
                    int rowY = Convert.ToInt32(frame.Key.RowY);

                    string s1 = colX.ToExcelColumDescription() + rowX.ToString();
                    string s2 = colY.ToExcelColumDescription() + rowY.ToString();
                    string val = string.Join(":", s1, s2);

                    if (frame.Value != OfficeOpenXml.Style.ExcelBorderStyle.None)
                        ws.Cells[val].Style.Border.BorderAround(frame.Value, System.Drawing.Color.Black);
                }
            }
            pck.SaveAs(newFile);
        }
        public static bool FileExists(ref string path, string fileName)
        {
            if (string.IsNullOrEmpty(path) || string.IsNullOrEmpty(fileName))
                throw new NullReferenceException("FileExists");

            IsPathValid(ref path);

            if (System.IO.File.Exists(Path.Combine(path, fileName)))
                return true;

            return false;
        }
        public static bool IsPathValid(ref string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return false;

            foreach (char chr in path)
                if (Path.GetInvalidPathChars().Contains(chr))
                    return false;

            if (!Directory.Exists(path))
                return false;

            path = path.EndsWith(@"\") ? path : path + @"\";

            return true;
        }
        #endregion
    }
}
